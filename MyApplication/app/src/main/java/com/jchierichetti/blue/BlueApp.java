package com.jchierichetti.blue;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.jchierichetti.blue.model.Coin;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import java.util.Date;

/**
 * Created by Admin on 18/08/2015.
 */
public class BlueApp extends Application {


    public static final Coin[] COINS = new Coin[]{new Coin(R.string.blue,"DollarBlue"),
            new Coin(R.string.official,"DollarOfficial")};

    public static final double DOLLAR_SAVING_PERCENTAGE = 1.2;
    public static final double DOLLAR_CREDIT_CARD_PERCENTAGE = 1.35;
    public static final int DEFAULT_HOUR =  9;

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onCreate() {
        super.onCreate();

        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);

        Parse.initialize(this, "GLQHAkdS2G5Etr8CbtdQBDuBoaJrxLo4BRwAnUqE", "9ESbvlH4lmpDolHlWD9DOGL9lL9AVUYS2bdZaauK");
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        if(installation.get(ParseConstants.ENABLE_NOTIFICATIONS)==null)
        {
            installation.put(ParseConstants.ENABLE_NOTIFICATIONS,true);
            installation.put(ParseConstants.NOTIFICATION_HOUR,DEFAULT_HOUR);
            installation.put(ParseConstants.ENABLE_DAILY_NOTIFICATIONS,true);
            for(Coin coin : COINS )
            {
                installation.put(String.format(ParseConstants.ENABLE_BY_VALUE_NOTIFICATION,coin.getParseKey()),false);
                installation.put(String.format(ParseConstants.ENABLE_VARIATION_NOTIFICATION,coin.getParseKey()),false);
                installation.put(String.format(ParseConstants.BY_VALUE_TYPE,coin.getParseKey()),ParseConstants.BUY_TYPE);
            }


        }

        installation.saveInBackground();


        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(1800);

        tracker = analytics.newTracker("UA-53657748-2");
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);




    }
}
