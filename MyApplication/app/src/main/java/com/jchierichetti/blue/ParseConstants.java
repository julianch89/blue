package com.jchierichetti.blue;

/**
 * Created by Admin on 21/08/2015.
 */
public class ParseConstants {

    public static final String ENABLE_NOTIFICATIONS = "enableNotifications";
    public static final String ENABLE_DAILY_NOTIFICATIONS = "enableDailyNotification";


    public static final String ENABLE_BY_VALUE_NOTIFICATION = "enable%sByValueNotification";
    public static final String BY_VALUE_NOTIFICATION = "%sByValueNotification";
    public static final String ENABLE_VARIATION_NOTIFICATION = "enable%sVariationNotification";
    public static final String VARIATION_NOTIFICATION = "%sVariationNotification";
    public static final String BY_VALUE_TYPE = "%sByValueNotificationType";



    public static final String NOTIFICATION_HOUR = "notificationHour";
    public static final String NOTIFICATION_MINUTES = "notificationMinutes";
    public static final String PRICES_TABLE = "CotizacionActual";
    public static final String HISTORY_TABLE = "CotizacionHistorica";
    public static final String PRICES_TABLE_TYPE_KEY = "type";
    public static final String PRICES_TABLE_LAST_VALUE_KEY = "lastValue";
    public static final String CREATE_AT_KEY = "createdAt";
    public static final String PRICES_TABLE_SELL_KEY = "valueSell";
    public static final String PRICES_TABLE_BUY_KEY = "valueBuy";
    public static final String PRICES_TABLE_AVG_KEY = "valueAvg";
    public static final String VALUE_DATE  ="date";

    public static final String DOLAR_BLUE_TYPE = "dolar_blue";
    public static final String DOLAR_OFFICIAL_TYPE = "dolar_oficial";
    public static final String EURO_BLUE_TYPE = "euro_blue";
    public static final String EURO_OFFICIAL_TYPE = "euro_oficial";

    public static final String BUY_TYPE = "buy";
    public static final String SELL_TYPE = "sell";
}
