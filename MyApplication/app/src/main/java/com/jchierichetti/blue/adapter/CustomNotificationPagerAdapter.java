package com.jchierichetti.blue.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.jchierichetti.blue.BlueApp;
import com.jchierichetti.blue.fragment.CustomNotificationFragment;

/**
 * Created by Admin on 24/08/2015.
 */
public class CustomNotificationPagerAdapter extends FragmentStatePagerAdapter {


    private FragmentManager mFragmentManager;
    private Context mContext;


    public CustomNotificationPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mFragmentManager = fm;
        mContext = context;
    }

    @Override
    public Fragment getItem(int position)
    {
        return CustomNotificationFragment.newInstance(BlueApp.COINS[position]);
    }

    @Override
    public int getCount() {
        return BlueApp.COINS.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getString(BlueApp.COINS[position].getResourceName());
    }

}
