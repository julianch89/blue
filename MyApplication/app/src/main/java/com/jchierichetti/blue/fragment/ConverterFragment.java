package com.jchierichetti.blue.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.jchierichetti.blue.BlueApp;
import com.jchierichetti.blue.MainActivity;
import com.jchierichetti.blue.ParseConstants;
import com.jchierichetti.blue.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 08/09/2015.
 */
public class ConverterFragment extends Fragment {


    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ConverterFragment newInstance(int sectionNumber)
    {
        ConverterFragment fragment = new ConverterFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    private double mDollarBlueValue;
    private double mDollarOffialValue;
    @Bind(R.id.converter_pesos_edittext)
    EditText mPesosEditText;
    @Bind(R.id.converter_dolar_blue_edittext)
    EditText mDolarBlueEditText;
    @Bind(R.id.converter_dolar_oficial_edittext)
    EditText mDolarOficialText;
    @Bind(R.id.converter_dolar_credit_card_edittext)
    EditText mDolarCreditCardEditText;
    @Bind(R.id.converter_dolar_saving_edittext)
    EditText mDolarSavingEditText;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view  =  inflater.inflate(R.layout.fragment_converter, container, false);
        ButterKnife.bind(this, view);
        getPriceValues();

        return view;
    }

    private void getPriceValues()
    {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(ParseConstants.PRICES_TABLE);
        query.addDescendingOrder(ParseConstants.CREATE_AT_KEY);
        query.setLimit(2);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> cotizacionList, ParseException e) {
                if (e == null) {
                    for (ParseObject item : cotizacionList) {
                        switch (item.getString(ParseConstants.PRICES_TABLE_TYPE_KEY)) {
                            case (ParseConstants.DOLAR_BLUE_TYPE):
                                mDollarBlueValue = item.getDouble(ParseConstants.PRICES_TABLE_SELL_KEY);
                                break;
                            case (ParseConstants.DOLAR_OFFICIAL_TYPE):
                                mDollarOffialValue = item.getDouble(ParseConstants.PRICES_TABLE_SELL_KEY);
                                break;

                        }

                    }
                    initializeEditTexts();


                }
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }


    private boolean mPesosdLock =  true;
    private boolean mDolarBlueLock = true;
    private boolean mDolarOficialLock = true;
    private boolean mDolarCreditCardLock = true;
    private boolean mDolarSavingLock = true;

    private void enableEditTexts(boolean enable)
    {
        mPesosdLock = enable;
        mDolarBlueLock = enable;
        mDolarOficialLock = enable;
        mDolarCreditCardLock = enable;
        mDolarSavingLock = enable;
    }
    private void clearEditTexts(EditText actualView)
    {
        if(actualView != mPesosEditText)mPesosEditText.setText("");
        if(actualView != mDolarBlueEditText)mDolarBlueEditText.setText("");
        if(actualView != mDolarOficialText)mDolarOficialText.setText("");
        if(actualView != mDolarCreditCardEditText)mDolarCreditCardEditText.setText("");
        if(actualView != mDolarSavingEditText)mDolarSavingEditText.setText("");
    }

    private void initializeEditTexts()
    {
        mPesosEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!mPesosdLock) return;
                if(s.length()==0) {
                    enableEditTexts(false);
                    clearEditTexts(mPesosEditText);
                    enableEditTexts(true);
                    return;
                }
                enableEditTexts(false);
                mDolarOficialText.setText(String.format(Locale.US,"%.2f",Double.valueOf(s.toString()) / mDollarOffialValue));
                mDolarBlueEditText.setText(String.format(Locale.US,"%.2f",round(Double.valueOf(s.toString()) / mDollarBlueValue, 3)));
                mDolarCreditCardEditText.setText(String.format(Locale.US,"%.2f",round(Double.valueOf(s.toString()) / (mDollarOffialValue * BlueApp.DOLLAR_CREDIT_CARD_PERCENTAGE), 3)));
                mDolarSavingEditText.setText(String.format(Locale.US,"%.2f",round(Double.valueOf(s.toString()) / (mDollarOffialValue * BlueApp.DOLLAR_SAVING_PERCENTAGE), 3)));
                enableEditTexts(true);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        mDolarBlueEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!mDolarBlueLock) return;
                if(s.length()==0) {
                    enableEditTexts(false);
                    clearEditTexts(mDolarBlueEditText);
                    enableEditTexts(true);
                    return;
                }
                enableEditTexts(false);
                mDolarOficialText.setText(String.format(Locale.US,"%.2f",(Double.valueOf(s.toString()) * mDollarBlueValue) / mDollarOffialValue));
                mPesosEditText.setText(String.format(Locale.US,"%.2f",Double.valueOf(s.toString())*mDollarBlueValue));
                mDolarCreditCardEditText.setText(String.format(Locale.US,"%.2f",(Double.valueOf(s.toString())*mDollarBlueValue)/(mDollarOffialValue*BlueApp.DOLLAR_CREDIT_CARD_PERCENTAGE)));
                mDolarSavingEditText.setText(String.format(Locale.US,"%.2f",(Double.valueOf(s.toString())*mDollarBlueValue)/(mDollarOffialValue* BlueApp.DOLLAR_SAVING_PERCENTAGE)));
                enableEditTexts(true);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        mDolarOficialText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!mDolarOficialLock) return;
                if(s.length()==0) {
                    enableEditTexts(false);
                    clearEditTexts(mDolarOficialText);
                    enableEditTexts(true);
                    return;
                }
                enableEditTexts(false);
                mDolarBlueEditText.setText(String.format(Locale.US, "%.2f", round((Double.valueOf(s.toString()) * mDollarOffialValue) / mDollarBlueValue, 3)));
                mPesosEditText.setText(String.format(Locale.US, "%.2f", round(Double.valueOf(s.toString()) * mDollarOffialValue, 3)));
                mDolarCreditCardEditText.setText(String.format(Locale.US, "%.2f", round((Double.valueOf(s.toString()) * mDollarOffialValue) / (mDollarOffialValue * BlueApp.DOLLAR_CREDIT_CARD_PERCENTAGE), 3)));
                mDolarSavingEditText.setText(String.format(Locale.US, "%.2f", round((Double.valueOf(s.toString()) * mDollarOffialValue) / (mDollarOffialValue * BlueApp.DOLLAR_SAVING_PERCENTAGE), 3)));
                enableEditTexts(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mDolarCreditCardEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!mDolarCreditCardLock) return;
                if(s.length()==0) {
                    enableEditTexts(false);
                    clearEditTexts(mDolarCreditCardEditText);
                    enableEditTexts(true);
                    return;
                }
                enableEditTexts(false);
                mDolarBlueEditText.setText(String.format(Locale.US, "%.2f", round((Double.valueOf(s.toString()) * (mDollarOffialValue * BlueApp.DOLLAR_CREDIT_CARD_PERCENTAGE)) / mDollarBlueValue, 3)));
                mPesosEditText.setText(String.format(Locale.US, "%.2f", round(Double.valueOf(s.toString()) * (mDollarOffialValue * BlueApp.DOLLAR_CREDIT_CARD_PERCENTAGE), 3)));
                mDolarOficialText.setText(String.format(Locale.US, "%.2f", round(Double.valueOf(s.toString()) / BlueApp.DOLLAR_CREDIT_CARD_PERCENTAGE, 3)));
                mDolarSavingEditText.setText(String.format(Locale.US, "%.2f", round(Double.valueOf(s.toString()) * BlueApp.DOLLAR_CREDIT_CARD_PERCENTAGE / BlueApp.DOLLAR_SAVING_PERCENTAGE, 3)));
                enableEditTexts(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mDolarSavingEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!mDolarSavingLock) return;
                if(s.length()==0) {
                    enableEditTexts(false);
                    clearEditTexts(mDolarSavingEditText);
                    enableEditTexts(true);
                    return;
                }
                enableEditTexts(false);
                mDolarBlueEditText.setText(String.format(Locale.US, "%.2f", round((Double.valueOf(s.toString()) * (mDollarOffialValue * BlueApp.DOLLAR_SAVING_PERCENTAGE)) / mDollarBlueValue, 3)));
                mPesosEditText.setText(String.format(Locale.US, "%.2f", round(Double.valueOf(s.toString()) * (mDollarOffialValue * BlueApp.DOLLAR_SAVING_PERCENTAGE), 3)));
                mDolarOficialText.setText(String.format(Locale.US, "%.2f", round(Double.valueOf(s.toString()) / BlueApp.DOLLAR_SAVING_PERCENTAGE, 3)));
                mDolarCreditCardEditText.setText(String.format(Locale.US, "%.2f", round(Double.valueOf(s.toString()) * BlueApp.DOLLAR_SAVING_PERCENTAGE / BlueApp.DOLLAR_CREDIT_CARD_PERCENTAGE, 3)));
                enableEditTexts(true);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
