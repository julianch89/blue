Parse.Cloud.job("saveDailyValues", function(request, status) {
    Parse.Cloud.httpRequest({
        url: 'http://api.bluelytics.com.ar/v2/latest'
    }).then(function(httpResponse) {
        // success
        var cotizacionJson = JSON.parse(httpResponse.text);
        var Cotizacion = Parse.Object.extend("Cotizacion");
        var cotizacion = new Cotizacion();

        cotizacion.set("type", "dolar_blue");
        cotizacion.set("valueAvg", cotizacionJson.blue.value_avg);
        cotizacion.set("valueSell", cotizacionJson.blue.value_sell);
        cotizacion.set("valueBuy", cotizacionJson.blue.value_buy);
        cotizacion.set("date", cotizacionJson.last_update);
        cotizacion.save(null, {
            success: function(cotizacion) {
                // Execute any logic that should take place after the object is saved.
                alert('New object created with objectId: ' + cotizacion.id);
            },
            error: function(cotizacion, error) {
                // Execute any logic that should take place if the save fails.
                // error is a Parse.Error with an error code and message.
                alert('Failed to create new object, with error code: ' + error.message);
            }
        });

        var cotizacion = new Cotizacion();

        cotizacion.set("type", "dolar_oficial");
        cotizacion.set("valueAvg", cotizacionJson.oficial.value_avg);
        cotizacion.set("valueSell", cotizacionJson.oficial.value_sell);
        cotizacion.set("valueBuy", cotizacionJson.oficial.value_buy);
        cotizacion.set("date", cotizacionJson.last_update);
        cotizacion.save(null, {
            success: function(cotizacion) {
                // Execute any logic that should take place after the object is saved.
                alert('New object created with objectId: ' + cotizacion.id);
            },
            error: function(cotizacion, error) {
                // Execute any logic that should take place if the save fails.
                // error is a Parse.Error with an error code and message.
                alert('Failed to create new object, with error code: ' + error.message);
            }
        });

        var cotizacion = new Cotizacion();

        cotizacion.set("type", "euro_blue");
        cotizacion.set("valueAvg", cotizacionJson.blue_euro.value_avg);
        cotizacion.set("valueSell", cotizacionJson.blue_euro.value_sell);
        cotizacion.set("valueBuy", cotizacionJson.blue_euro.value_buy);
        cotizacion.set("date", cotizacionJson.last_update);
        cotizacion.save(null, {
            success: function(cotizacion) {
                // Execute any logic that should take place after the object is saved.
                alert('New object created with objectId: ' + cotizacion.id);
            },
            error: function(cotizacion, error) {
                // Execute any logic that should take place if the save fails.
                // error is a Parse.Error with an error code and message.
                alert('Failed to create new object, with error code: ' + error.message);
            }
        });

        var cotizacion = new Cotizacion();

        cotizacion.set("type", "euro_oficial");
        cotizacion.set("valueAvg", cotizacionJson.oficial_euro.value_avg);
        cotizacion.set("valueSell", cotizacionJson.oficial_euro.value_sell);
        cotizacion.set("valueBuy", cotizacionJson.oficial_euro.value_buy);
        cotizacion.set("date", cotizacionJson.last_update);
        cotizacion.save(null, {
            success: function(cotizacion) {
                // Execute any logic that should take place after the object is saved.
                alert('New object created with objectId: ' + cotizacion.id);
            },
            error: function(cotizacion, error) {
                // Execute any logic that should take place if the save fails.
                // error is a Parse.Error with an error code and message.
                alert('Failed to create new object, with error code: ' + error.message);
            }
        });

        console.log(httpResponse.text);
        status.success(httpResponse.text);
    }, function(httpResponse) {
        // error
        console.error('Request failed with response code ' + httpResponse.status);
        status.error(httpResponse.status);
    });
});


Parse.Cloud.job("saveDailyValues2", function(request, status) {
    Parse.Cloud.httpRequest({
        url: 'http://api.bluelytics.com.ar/json/last_price'
    }).then(function(httpResponse) {

        // success
        var cotizacionJson = JSON.parse(httpResponse.text);
        var cotizacionBlueSell = 0;
        var cotizacionBlueBuy = 0;
        var cotizacionBlueAvg = 0;
        var cotizacionOficialSell = 0;
        var cotizacionOficialBuy = 0;
        var cotizacionOficialAvg = 0;
        var cotizacionBlueCount = 0;
        var date;
        for (var i = 0; i < cotizacionJson.length; i++) {
            if (cotizacionJson[i].source != "oficial") {
                cotizacionBlueSell += cotizacionJson[i].value_sell;
                cotizacionBlueBuy += cotizacionJson[i].value_buy;
                cotizacionBlueAvg += cotizacionJson[i].value_avg;
                cotizacionBlueCount++;
            } else {
                cotizacionOficialSell = cotizacionJson[i].value_sell;
                cotizacionOficialBuy = cotizacionJson[i].value_buy;
                cotizacionOficialAvg = cotizacionJson[i].value_avg;
            }
            date = cotizacionJson[i].date;
        }
        cotizacionBlueSell = cotizacionBlueSell / cotizacionBlueCount;
        cotizacionBlueBuy = cotizacionBlueBuy / cotizacionBlueCount;
        cotizacionBlueAvg = cotizacionBlueAvg / cotizacionBlueCount;

        var Cotizacion = Parse.Object.extend("CotizacionHistorica");
        var cotizacion = new Cotizacion();

        cotizacion.set("type", "dolar_blue");
        cotizacion.set("valueSell", cotizacionBlueSell);
        cotizacion.set("valueBuy", cotizacionBlueBuy);

        var res = date.split("-");
        var dateFormated = parseInt(res[1], 10) + "/" + parseInt(res[2].substring(0, 2), 10) + "/" + res[0];
        cotizacion.set("date", dateFormated);
        cotizacion.save(null, {
            success: function(cotizacion) {
                // Execute any logic that should take place after the object is saved.
                alert('New object created with objectId: ' + cotizacion.id);
            },
            error: function(cotizacion, error) {
                // Execute any logic that should take place if the save fails.
                // error is a Parse.Error with an error code and message.
                alert('Failed to create new object, with error code: ' + error.message);
            }
        });

        var cotizacion = new Cotizacion();

        cotizacion.set("type", "dolar_oficial");
        cotizacion.set("valueSell", cotizacionOficialSell);
        cotizacion.set("valueBuy", cotizacionOficialBuy);
        cotizacion.set("date", dateFormated);
        cotizacion.save(null, {
            success: function(cotizacion) {
                // Execute any logic that should take place after the object is saved.
                alert('New object created with objectId: ' + cotizacion.id);
            },
            error: function(cotizacion, error) {
                // Execute any logic that should take place if the save fails.
                // error is a Parse.Error with an error code and message.
                alert('Failed to create new object, with error code: ' + error.message);
            }
        });

        console.log(httpResponse.text);
        status.success(httpResponse.text);
    }, function(httpResponse) {
        // error
        console.error('Request failed with response code ' + httpResponse.status);
        status.error(httpResponse.status);
    });
});



Parse.Cloud.job("updateActualValue", function(request, status) {
    Parse.Cloud.useMasterKey();
    Parse.Cloud.httpRequest({
            url: 'http://api.bluelytics.com.ar/json/last_price'
        }).then(function(httpResponse) {
                // success
                var cotizacionJson = JSON.parse(httpResponse.text);
                var cotizacionBlueSell = 0;
                var cotizacionBlueBuy = 0;
                var cotizacionBlueAvg = 0;
                var cotizacionOficialSell = 0;
                var cotizacionOficialBuy = 0;
                var cotizacionOficialAvg = 0;
                var cotizacionBlueCount = 0;
                var date;
                for (var i = 0; i < cotizacionJson.length; i++) {
                    if (cotizacionJson[i].source != "oficial") {
                        cotizacionBlueSell += cotizacionJson[i].value_sell;
                        cotizacionBlueBuy += cotizacionJson[i].value_buy;
                        cotizacionBlueAvg += cotizacionJson[i].value_avg;
                        cotizacionBlueCount++;
                    } else {
                        cotizacionOficialSell = cotizacionJson[i].value_sell;
                        cotizacionOficialBuy = cotizacionJson[i].value_buy;
                        cotizacionOficialAvg = cotizacionJson[i].value_avg;
                    }
                    updateDate = cotizacionJson[i].date;
                }
                cotizacionBlueSell = cotizacionBlueSell / cotizacionBlueCount;
                cotizacionBlueBuy = cotizacionBlueBuy / cotizacionBlueCount;
                cotizacionBlueAvg = cotizacionBlueAvg / cotizacionBlueCount;




                var ActualValue = Parse.Object.extend("CotizacionActual");

                var query = new Parse.Query(ActualValue);
                query.find({
                        success: function(results) {
                            for (var i = 0; i < results.length; i++) {
                                results[i].destroy({});
                            }


                            var lastDolarQuery = new Parse.Query("CotizacionHistorica");
                            lastDolarQuery.descending("createdAt");
                            lastDolarQuery.limit(2);
                            lastDolarQuery.find({
                                    success: function(lastDolarQuery) {

                                        for (var i = 0; i < lastDolarQuery.length; i++) {
                                            var object = lastDolarQuery[i];
                                            if (object.get("type") == "dolar_oficial")
                                                var dollarOfficialLastValue = object.get('valueSell');
                                            else
                                                var dollarBlueLastValue = object.get('valueSell');
                                        }
                                        var Cotizacion = Parse.Object.extend("CotizacionActual");
                                        var cotizacion = new Cotizacion();
                                        cotizacion.set("type", "dolar_blue");
                                        cotizacion.set("valueAvg", cotizacionBlueAvg);
                                        cotizacion.set("valueSell", cotizacionBlueSell);
                                        cotizacion.set("valueBuy", cotizacionBlueBuy);
                                        cotizacion.set("lastValue", dollarBlueLastValue);
                                        cotizacion.set("date", updateDate);
                                        cotizacion.save(null, {
                                            success: function(cotizacion) {
                                                // Execute any logic that should take place after the object is saved.
                                                alert('New object created with objectId: ' + cotizacion.id);
                                            },
                                            error: function(cotizacion, error) {
                                                // Execute any logic that should take place if the save fails.
                                                // error is a Parse.Error with an error code and message.
                                                alert('Failed to create new object, with error code: ' + error.message);
                                            }
                                        });

                                        var cotizacion = new Cotizacion();

                                        cotizacion.set("type", "dolar_oficial");
                                        cotizacion.set("valueAvg", cotizacionOficialAvg);
                                        cotizacion.set("valueSell", cotizacionOficialSell);
                                        cotizacion.set("valueBuy", cotizacionOficialBuy);
                                        cotizacion.set("lastValue", dollarOfficialLastValue);
                                        cotizacion.set("date", updateDate);
                                        cotizacion.save(null, {
                                            success: function(cotizacion) {
                                                // Execute any logic that should take place after the object is saved.
                                                alert('New object created with objectId: ' + cotizacion.id);
                                            },
                                            error: function(cotizacion, error) {
                                                // Execute any logic that should take place if the save fails.
                                                // error is a Parse.Error with an error code and message.
                                                alert('Failed to create new object, with error code: ' + error.message);
                                            }
                                        });
                                        var serverTime = new Date();
                                        console.log("server hour : " + serverTime.getHours());
                                        serverTime.setHours(serverTime.getHours() - 3);
										var oneDayAway = serverTime;
										oneDayAway.setDate(oneDayAway.getDate() + 1);


                                        //DOLAR BLUE NOTIFICATION
                                        var dif = cotizacionBlueSell - dollarBlueLastValue;
                                        var queryInstallation = new Parse.Query(Parse.Installation);
                                        queryInstallation.equalTo('enableDollarBlueVariationNotification', true);
										queryInstallation.equalTo('enableNotifications', true);
                                        queryInstallation.lessThanOrEqualTo("DollarBlueVariationNotification", 0);
										queryInstallation.equalTo('notificationHour',serverTime.getHours());
                                        queryInstallation.greaterThan("DollarBlueVariationNotification", dif);

                                        var queryInstallation2 = new Parse.Query(Parse.Installation);
                                        queryInstallation2.equalTo('enableDollarBlueVariationNotification', true);
										queryInstallation2.equalTo('enableNotifications', true);
                                        queryInstallation2.greaterThan("DollarBlueVariationNotification", 0);
										queryInstallation2.equalTo('notificationHour',serverTime.getHours());
                                        queryInstallation2.lessThanOrEqualTo("DollarBlueVariationNotification", dif);

                                        Parse.Push.send({
                                            where: Parse.Query.or(queryInstallation, queryInstallation2),
											expiration_time: oneDayAway,
                                            data: {
                                                alert: "Variación del dolar blue \nCompra: $" + cotizacionBlueBuy.toFixed(3) + " Venta: $" + cotizacionBlueSell.toFixed(3)
                                            },

                                        }, {
                                            success: function() {
                                                // Push was successful
                                            },
                                            error: function(error) {
                                                // Handle error
                                            }
                                        });
										
										//DOLAR BLUE BY VALUE NOTIFICATION
                                        var queryInstallation = new Parse.Query(Parse.Installation);
                                        queryInstallation.equalTo('enableDollarBlueByValueNotification', true);
										queryInstallation.equalTo('enableNotifications', true);
                                        queryInstallation.lessThanOrEqualTo("DollarBlueByValueNotification", cotizacionBlueSell);
										queryInstallation.equalTo('notificationHour',serverTime.getHours());
                                        queryInstallation.greaterThan("DollarBlueByValueNotification", dollarBlueLastValue);

                                        var queryInstallation2 = new Parse.Query(Parse.Installation);
                                        queryInstallation2.equalTo('enableDollarBlueByValueNotification', true);
										queryInstallation2.equalTo('enableNotifications', true);
                                        queryInstallation2.lessThanOrEqualTo("DollarBlueByValueNotification", dollarBlueLastValue);
										queryInstallation2.equalTo('notificationHour',serverTime.getHours());
                                        queryInstallation2.greaterThan("DollarBlueByValueNotification", cotizacionBlueSell);

                                        Parse.Push.send({
                                            where: Parse.Query.or(queryInstallation, queryInstallation2),
											expiration_time: oneDayAway,
                                            data: {
                                                alert: "Valor del dolar blue \nCompra: $" + cotizacionBlueBuy.toFixed(3) + " Venta: $" + cotizacionBlueSell.toFixed(3)
                                            },

                                        }, {
                                            success: function() {
                                                // Push was successful
                                            },
                                            error: function(error) {
                                                // Handle error
                                            }
                                        });


                                       


                                        //DOLAR OFICIAL NOTIFICATION
                                        var dif = cotizacionOficialSell - dollarOfficialLastValue;
                                        var queryInstallation = new Parse.Query(Parse.Installation);
                                        queryInstallation.equalTo('enableDollarOfficialVariationNotification', true);
										queryInstallation.equalTo('enableNotifications', true);
                                        queryInstallation.lessThanOrEqualTo("DollarOfficialVariationNotification", 0);
										queryInstallation.equalTo('notificationHour',serverTime.getHours());
                                        queryInstallation.greaterThan("DollarOfficialVariationNotification", dif);

                                        var queryInstallation2 = new Parse.Query(Parse.Installation);
                                        queryInstallation2.equalTo('enableDollarOfficialVariationNotification', true);
										queryInstallation2.equalTo('enableNotifications', true);
                                        queryInstallation2.greaterThan("DollarOfficialVariationNotification", 0);
										queryInstallation2.equalTo('notificationHour',serverTime.getHours());
                                        queryInstallation2.lessThanOrEqualTo("DollarOfficialVariationNotification", dif);

                                        Parse.Push.send({
                                            where: Parse.Query.or(queryInstallation, queryInstallation2),
											expiration_time: oneDayAway,
                                            data: {
                                                alert: "Variación del dolar oficial \nCompra: $" + cotizacionOficialBuy.toFixed(3) + " Venta: $" + cotizacionOficialSell.toFixed(3)
                                            }
                                        }, {
                                            success: function() {
                                                // Push was successful
                                            },
                                            error: function(error) {
                                                // Handle error
                                            }
										});
										
										//DOLAR OFICIAL BY VALUE NOTIFICATION
                                        var queryInstallation = new Parse.Query(Parse.Installation);
                                        queryInstallation.equalTo('enableDollarOfficialByValueNotification', true);
										queryInstallation.equalTo('enableNotifications', true);
                                        queryInstallation.lessThanOrEqualTo("DollarOfficialByValueNotification", cotizacionOficialSell);
										queryInstallation.equalTo('notificationHour',serverTime.getHours());
                                        queryInstallation.greaterThan("DollarOfficialByValueNotification", dollarOfficialLastValue);

                                        var queryInstallation2 = new Parse.Query(Parse.Installation);
                                        queryInstallation2.equalTo('enableDollarOfficialByValueNotification', true);
										queryInstallation2.equalTo('enableNotifications', true);
                                        queryInstallation2.lessThanOrEqualTo("DollarOfficialByValueNotification", dollarOfficialLastValue);
										queryInstallation2.equalTo('notificationHour',serverTime.getHours());
                                        queryInstallation2.greaterThan("DollarOfficialByValueNotification", cotizacionOficialSell);

                                        Parse.Push.send({
                                            where: Parse.Query.or(queryInstallation, queryInstallation2),
											expiration_time: oneDayAway,
                                            data: {
                                                alert: "Valor del dolar oficial \nCompra: $" + cotizacionBlueBuy.toFixed(3) + " Venta: $" + cotizacionBlueSell.toFixed(3)
                                            },

                                        }, {
                                            success: function() {
                                                // Push was successful
                                            },
                                            error: function(error) {
                                                // Handle error
                                            }
                                        });
										

                                        console.log("DAILY NOTIFICATION");
                                        // DAILY NOTIFICATION 
                                        var dailyNotificationQuery = new Parse.Query(Parse.Installation);
                                        dailyNotificationQuery.equalTo('enableNotifications', true);
										dailyNotificationQuery.equalTo('enableDailyNotification', true);
										dailyNotificationQuery.equalTo('notificationHour',serverTime.getHours());
                                        Parse.Push.send({
                                            where: dailyNotificationQuery, // Set our Installation query
											expiration_time: oneDayAway,
                                            data: {
                                                alert: "Dolar BLUE \nCompra: $" + cotizacionBlueBuy.toFixed(3) + " Venta: $" + cotizacionBlueSell.toFixed(3) + "\nDolar OFICIAL \nCompra: $" + cotizacionOficialBuy.toFixed(3) + " Venta: $" + cotizacionOficialSell.toFixed(3)
                                            }
                                        }, {
                                            success: function() {
                                                // Push was successful
                                            },
                                            error: function(error) {
                                                // Handle error
                                            }
                                        });

                                    
                                    status.success(httpResponse.text);

                                },
                                error: function(error) {
                                    alert("Error: " + error.code + " " + error.message);
                                }
                            });




                    },
                    error: function(error) {
                        alert("Error: " + error.code + " " + error.message);
                    }
                });

        },
        function(httpResponse) {
            // error
            status.error(httpResponse.status);
        });


});