package com.jchierichetti.blue.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.LocalActivityManager;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TimePicker;


import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.jchierichetti.blue.BlueApp;
import com.jchierichetti.blue.CustomViewPager;
import com.jchierichetti.blue.MainActivity;
import com.jchierichetti.blue.ParseConstants;
import com.jchierichetti.blue.R;
import com.jchierichetti.blue.adapter.CustomNotificationPagerAdapter;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 20/08/2015.
 */
public class SettingsFragment extends Fragment {

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    @Bind(R.id.settings_main_switch)
    Switch mMainSwitch;
    @Bind(R.id.settings_actual_value_switch)
    Switch mActualValueSwitch;
    @Bind(R.id.settings_actual_value_title)
    TextView mActualValueTitle;
    @Bind(R.id.settings_actual_value_text)
    TextView mActualValueText;
    @Bind(R.id.settings_time_button)
    Button mTimeButton;
    @Bind(R.id.settings_time_title)
    TextView mTimeTitle;
    @Bind(R.id.settings_time_value)
    TextView mTimeValue;
    @Bind(R.id.pager)
    CustomViewPager mViewPager;
    @Bind(R.id.pager_title_strip)
    PagerTitleStrip mPagerTitleStrip;

    private ParseInstallation mInstallation;
    private int mUserNotificationHour;


    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */

    public static SettingsFragment newInstance(int sectionNumber) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);
        mInstallation =  ParseInstallation.getCurrentInstallation();
        mViewPager.setAdapter(new CustomNotificationPagerAdapter(getActivity().getSupportFragmentManager(),getActivity()));

        initializeSwitchers();
        initializeTimeDialog();

        return view;
    }



    @Override
    public void onStart() {
        super.onStart();
        Tracker tracker = BlueApp.tracker;
        tracker.setScreenName("settings");
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("UX")
                .setAction("click")
                .setLabel("enter settings")
                .build());

        Map<String, String> dimensions = new HashMap<String, String>();

        dimensions.put("enterSettings", "true");
        ParseAnalytics.trackEvent("Settings", dimensions);
    }

    private void initializeTimeDialog()
    {
        mUserNotificationHour = mInstallation.getInt(ParseConstants.NOTIFICATION_HOUR);
        String userHour = String.valueOf(mUserNotificationHour);
        userHour = userHour.length() < 2 ? "0" + userHour : userHour;
        mTimeValue.setText(userHour+ ":00hs");
        mTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_hour_picker);
                final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.hour_picker_number);
                np.setMaxValue(23);
                np.setMinValue(0);
                np.setValue(mUserNotificationHour);
                np.setWrapSelectorWheel(false);
                dialog.findViewById(R.id.hour_cancel_button).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.findViewById(R.id.hour_accept_button).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String hour = String.valueOf(np.getValue());
                        mTimeValue.setText(hour + ":00hs");
                        mInstallation.put(ParseConstants.NOTIFICATION_HOUR, np.getValue());
                        mInstallation.saveInBackground();
                        mUserNotificationHour = np.getValue();
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    private void initializeSwitchers()
    {
        mMainSwitch.setChecked(mInstallation.getBoolean(ParseConstants.ENABLE_NOTIFICATIONS));
        mActualValueSwitch.setChecked(mInstallation.getBoolean(ParseConstants.ENABLE_DAILY_NOTIFICATIONS));
        mMainSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                enableContent(isChecked);
                mInstallation.put(ParseConstants.ENABLE_NOTIFICATIONS, isChecked);
                mInstallation.saveInBackground();
            }
        });
        mActualValueSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mInstallation.put(ParseConstants.ENABLE_DAILY_NOTIFICATIONS,isChecked);
                mInstallation.saveInBackground();
            }
        });

        enableContent(mMainSwitch.isChecked());
    }


    private void enableContent(boolean enable) {
        mActualValueText.setEnabled(enable);
        mActualValueTitle.setEnabled(enable);
        mActualValueSwitch.setEnabled(enable);
        mTimeButton.setEnabled(enable);
        mTimeTitle.setEnabled(enable);
        mTimeValue.setEnabled(enable);
        mPagerTitleStrip.setEnabled(enable);
        mViewPager.setSwipeable(enable);
        CustomNotificationFragment fragment = (CustomNotificationFragment)((CustomNotificationPagerAdapter) mViewPager.getAdapter()).instantiateItem(mViewPager, mViewPager.getCurrentItem());
        if(fragment !=null)fragment.setEnable(enable);
    }


}
