package com.jchierichetti.blue.model;

import java.io.Serializable;

/**
 * Created by admin on 8/24/2015.
 */
public class Coin implements Serializable {

    private int mResourceName;
    private String mParseKey;

    public Coin(int resourceName, String parseKey)
    {
        mResourceName = resourceName;
        mParseKey = parseKey;
    }

    public int getResourceName(){return mResourceName;}
    public String getParseKey(){return mParseKey;}
}
