package com.jchierichetti.blue;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.jchierichetti.blue.fragment.HomeFragment;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 08/09/2015.
 */
public class BlueAppWidgetProvider extends AppWidgetProvider {

    @Override
    public void onUpdate(Context context, final AppWidgetManager appWidgetManager, int[] appWidgetIds) {


        final int N = appWidgetIds.length;

        // Perform this loop procedure for each App Widget that belongs to this provider
        for (int i=0; i<N; i++) {
            final int appWidgetId = appWidgetIds[i];

            // Create an Intent to launch ExampleActivity
            Intent intent = new Intent(context, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

            // Get the layout for the App Widget and attach an on-click listener
            // to the button
            final RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
            views.setOnClickPendingIntent(R.id.widget_layout, pendingIntent);
            ParseQuery<ParseObject> query = ParseQuery.getQuery(ParseConstants.PRICES_TABLE);
            query.addDescendingOrder(ParseConstants.CREATE_AT_KEY);
            query.setLimit(2);
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> cotizacionList, ParseException e) {
                    if (e == null) {
                        for (ParseObject item : cotizacionList) {
                            switch (item.getString(ParseConstants.PRICES_TABLE_TYPE_KEY)) {
                                case (ParseConstants.DOLAR_BLUE_TYPE):

                                    views.setTextViewText(R.id.widget_dollar_blue_buy_value, HomeFragment.getValueFormated(HomeFragment.round(item.getDouble(ParseConstants.PRICES_TABLE_BUY_KEY), 3)));
                                    views.setTextViewText(R.id.widget_dollar_blue_sell_value, HomeFragment.getValueFormated(HomeFragment.round(item.getDouble(ParseConstants.PRICES_TABLE_SELL_KEY), 3)));
                                    break;
                                case (ParseConstants.DOLAR_OFFICIAL_TYPE):
                                    //actualValue = HomeFragment.round(item.getDouble(ParseConstants.PRICES_TABLE_SELL_KEY), 3);

                                    //getValueFormated(round(item.getDouble(ParseConstants.PRICES_TABLE_BUY_KEY), 3)));
                                    break;
                            }
                        }
                        appWidgetManager.updateAppWidget(appWidgetId, views);
                    }
                }
            });
            // Tell the AppWidgetManager to perform an update on the current app widget
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }
}
