package com.jchierichetti.blue.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.jchierichetti.blue.ParseConstants;
import com.jchierichetti.blue.R;
import com.jchierichetti.blue.model.Coin;
import com.parse.ParseInstallation;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 24/08/2015.
 */
public class CustomNotificationFragment extends Fragment {


    private static final String COIN_EXTRA_KEY = "COIN_EXTRA_KEY";
    private Coin mCoin;

    @Bind(R.id.notifications_custom_variation_title)
    TextView mVariationTitle;
    @Bind(R.id.notifications_custom_variation_switch)
    Switch mVariationSwitch;
    @Bind(R.id.notifications_custom_variation_text)
    TextView mVariationText;
    @Bind(R.id.notification_custom_variation_value)
    EditText mVariationValue;
    @Bind(R.id.notifications_custom_value_title)
    TextView mValueTitle;
    @Bind(R.id.notifications_custom_value_switch)
    Switch mValueSwitch;
    @Bind(R.id.notifications_custom_value_text)
    TextView mValueText;
    @Bind(R.id.notification_custom_byvalue_value)
    EditText mByValueValue;
    @Bind(R.id.notification_custom_buy_radiobutton)
    RadioButton mBuyRadioButton;
    @Bind(R.id.notification_custom_sell_radiobutton)
    RadioButton mSellRadionButton;
    @Bind(R.id.notification_custom_type_radio_group)
    RadioGroup mTypeRadioGroup;


    private ParseInstallation mInstallation;




    public static CustomNotificationFragment newInstance(Coin coin)
    {
        CustomNotificationFragment fragment  = new CustomNotificationFragment();
        Bundle args = new Bundle();
        args.putSerializable(COIN_EXTRA_KEY, coin);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_custom_notification, container, false);
        mCoin = (Coin)getArguments().getSerializable(COIN_EXTRA_KEY);
        ButterKnife.bind(this, rootView);
        mInstallation =  ParseInstallation.getCurrentInstallation();
        updateUI();
        initializeSwitchers();
        initializeEditTexs();
        return rootView;
    }

    private void initializeSwitchers()
    {
        mVariationSwitch.setChecked(mInstallation.getBoolean(String.format(ParseConstants.ENABLE_VARIATION_NOTIFICATION, mCoin.getParseKey())));
        mValueSwitch.setChecked(mInstallation.getBoolean(String.format(ParseConstants.ENABLE_BY_VALUE_NOTIFICATION, mCoin.getParseKey())));
        mVariationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mInstallation.put(String.format(ParseConstants.ENABLE_VARIATION_NOTIFICATION, mCoin.getParseKey()), isChecked);
                mInstallation.saveInBackground();
            }
        });
        mValueSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mInstallation.put(String.format(ParseConstants.ENABLE_BY_VALUE_NOTIFICATION, mCoin.getParseKey()), isChecked);
                mInstallation.saveInBackground();
            }
        });

        mBuyRadioButton.setChecked(mInstallation.getString(String.format(ParseConstants.BY_VALUE_TYPE, mCoin.getParseKey())).equals(ParseConstants.BUY_TYPE));
        mSellRadionButton.setChecked(mInstallation.getString(String.format(ParseConstants.BY_VALUE_TYPE,mCoin.getParseKey())).equals(ParseConstants.SELL_TYPE));
        mTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(mBuyRadioButton.isChecked())
                    mInstallation.put(String.format(ParseConstants.BY_VALUE_TYPE, mCoin.getParseKey()), ParseConstants.BUY_TYPE);
                else if(mSellRadionButton.isChecked())
                    mInstallation.put(String.format(ParseConstants.BY_VALUE_TYPE, mCoin.getParseKey()), ParseConstants.SELL_TYPE);
                mInstallation.saveInBackground();
            }
        });

    }


    private void initializeEditTexs()
    {
        double value = mInstallation.getDouble(String.format(ParseConstants.BY_VALUE_NOTIFICATION,mCoin.getParseKey()));
        if(value != 0)
            mByValueValue.setText(String.valueOf(value));
        mByValueValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) return;
                mInstallation.put(String.format(ParseConstants.BY_VALUE_NOTIFICATION, mCoin.getParseKey()), Double.valueOf(s.toString()));
                mInstallation.saveInBackground();
            }
        });
        value = mInstallation.getDouble(String.format(ParseConstants.VARIATION_NOTIFICATION,mCoin.getParseKey()));
        if(value!= 0 )
            mVariationValue.setText(String.valueOf(value));
        mVariationValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 0 ) return;
                mInstallation.put(String.format(ParseConstants.VARIATION_NOTIFICATION,mCoin.getParseKey()),Double.valueOf(s.toString()));
                mInstallation.saveInBackground();
            }
        });
    }

    private boolean isViewShown = false;
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
        {
            isViewShown  =true;
        }
        else
        {
            isViewShown = false;
        }
    }

    private boolean mIsEnable =true;

    public void setEnable(boolean isEnable)
    {
        mIsEnable = isEnable;
        if(isViewShown)
            updateUI();
    }
    private void updateUI()
    {
        mVariationTitle.setEnabled(mIsEnable);
        mVariationText.setEnabled(mIsEnable);
        mVariationSwitch.setEnabled(mIsEnable);
        mVariationValue.setEnabled(mIsEnable);
        mValueSwitch.setEnabled(mIsEnable);
        mValueText.setEnabled(mIsEnable);
        mValueTitle.setEnabled(mIsEnable);
        mByValueValue.setEnabled(mIsEnable);
        mBuyRadioButton.setEnabled(mIsEnable);
        mSellRadionButton.setEnabled(mIsEnable);
    }
}
