package com.jchierichetti.blue.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.jchierichetti.blue.BlueApp;
import com.jchierichetti.blue.MainActivity;
import com.jchierichetti.blue.ParseConstants;
import com.jchierichetti.blue.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 19/08/2015.
 */
public class HomeFragment extends Fragment{




    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static HomeFragment newInstance(int sectionNumber)
    {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Bind(R.id.home_dollar_official_sell_value)
    TextView mDollarOfficialSellValue;
    @Bind(R.id.home_dollar_official_buy_value)
    TextView mDollarOfficialBuyValue;
    @Bind(R.id.home_dollar_parallel_sell_value)
    TextView mDollarParallelSellValue;
    @Bind(R.id.home_dollar_parallel_buy_value)
    TextView mDollarParallelBuyValue;
    @Bind(R.id.home_dollar_saving_value)
    TextView mDollarSavingValue;
    @Bind(R.id.home_dollar_credit_card__value)
    TextView mDollarCreditCardValue;
    @Bind(R.id.home_date)
    TextView mDateTextView;
    @Bind(R.id.home_dollar_official_last_value)
    TextView mDollarOfficialLastValue;
    @Bind(R.id.home_dollar_official_variation_value)
    TextView mDollarOfficialVariation;
    @Bind(R.id.home_dollar_blue_last_value)
    TextView mDollarParallelLastValue;
    @Bind(R.id.home_dollar_blue_variation_value)
    TextView mDollarParallelVariation;
    @Bind(R.id.home_content)
    ScrollView mContentView;
    @Bind(R.id.home_progress_bar)
    ProgressBar mLoadingView;

    private String mDate;
    private int mShortAnimationDuration;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view  =  inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        mContentView.setAlpha(0f);
        mContentView.setVisibility(View.GONE);
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);
        updateValues();
        return view;
    }

    private void updateValues()
    {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(ParseConstants.PRICES_TABLE);
        query.addDescendingOrder(ParseConstants.CREATE_AT_KEY);
        query.setLimit(2);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> cotizacionList, ParseException e) {
                if (e == null) {
                    for (ParseObject item : cotizacionList) {
                        double actualValue;
                        double lastValue;
                        switch (item.getString(ParseConstants.PRICES_TABLE_TYPE_KEY)) {
                            case (ParseConstants.DOLAR_BLUE_TYPE):
                                actualValue = round(item.getDouble(ParseConstants.PRICES_TABLE_SELL_KEY), 3);
                                lastValue = round(item.getDouble(ParseConstants.PRICES_TABLE_LAST_VALUE_KEY), 3);
                                mDollarParallelSellValue.setText(getValueFormated(actualValue));
                                mDollarParallelBuyValue.setText(getValueFormated(round(item.getDouble(ParseConstants.PRICES_TABLE_BUY_KEY), 3)));
                                mDollarParallelLastValue.setText(getValueFormated(lastValue));
                                mDollarParallelVariation.setText(String.valueOf(round(((actualValue * 100) / lastValue) - 100, 2)) + "%");
                                break;
                            case (ParseConstants.DOLAR_OFFICIAL_TYPE):
                                actualValue = round(item.getDouble(ParseConstants.PRICES_TABLE_SELL_KEY), 3);
                                lastValue = round(item.getDouble(ParseConstants.PRICES_TABLE_LAST_VALUE_KEY), 3);
                                mDollarOfficialSellValue.setText(getValueFormated(actualValue));
                                mDollarOfficialBuyValue.setText(getValueFormated(round(item.getDouble(ParseConstants.PRICES_TABLE_BUY_KEY), 3)));
                                mDollarOfficialLastValue.setText(getValueFormated(lastValue));
                                mDollarOfficialVariation.setText(String.valueOf(round(((actualValue * 100) / lastValue) - 100, 2)) + "%");
                                mDollarSavingValue.setText(getValueFormated(round(item.getDouble(ParseConstants.PRICES_TABLE_SELL_KEY) * BlueApp.DOLLAR_SAVING_PERCENTAGE, 3)));
                                mDollarCreditCardValue.setText(getValueFormated(round(item.getDouble(ParseConstants.PRICES_TABLE_SELL_KEY) * BlueApp.DOLLAR_CREDIT_CARD_PERCENTAGE, 3)));
                                mDate = item.getString(ParseConstants.VALUE_DATE);
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
                                Date newDate = null;
                                try {
                                    newDate = format.parse(mDate);
                                    format = new SimpleDateFormat("dd/MM/yyyy HH:mm'hs'");
                                    mDateTextView.setText(format.format(newDate));

                                } catch (java.text.ParseException e1) {
                                    e1.printStackTrace();
                                }

                                break;
                        }
                    }
                    crossfade();
                }
            }
        });
    }

    private void crossfade() {

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        mContentView.setAlpha(0f);
        mContentView.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        mContentView.animate()
                .alpha(1f)
                .setDuration(mShortAnimationDuration)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        mLoadingView.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLoadingView.setVisibility(View.GONE);
                    }
                });
    }

    public static String getValueFormated(double value)
    {
        String valueString =  String.valueOf(value);
        String[] strings = valueString.split("\\.");
        for(int i=0;i<3-strings[1].length();i++)
            valueString+="0";
        return "$"+valueString;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sync:
                updateValues();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
