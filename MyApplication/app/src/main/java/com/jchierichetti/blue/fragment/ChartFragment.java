package com.jchierichetti.blue.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.graphics.Color;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;


import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.jchierichetti.blue.BlueApp;
import com.jchierichetti.blue.MainActivity;
import com.jchierichetti.blue.ParseConstants;
import com.jchierichetti.blue.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 19/08/2015.
 */
public class ChartFragment extends Fragment{


    @Bind(R.id.chart)
    LineChart mLineChart;
    @Bind(R.id.chart_dollar_blue_checkbox)
    CheckBox mDollarBlueCheckbox;
    @Bind(R.id.chart_dollar_official_checkbox)
    CheckBox mDollarOfficialCheckbox;
    @Bind(R.id.chart_content)
    LinearLayout mContentView;
    @Bind(R.id.chart_progress_bar)
    ProgressBar mLoadingView;


    private LineDataSet mDollarBlueDataSet;
    private LineDataSet mDollarOfficialDataSet;
    private int mShortAnimationDuration;
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */

    public static ChartFragment newInstance(int sectionNumber)
    {
        ChartFragment fragment = new ChartFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view  =  inflater.inflate(R.layout.fragment_chart,container,false);
        ButterKnife.bind(this, view);
        mContentView.setAlpha(0f);
        mContentView.setVisibility(View.GONE);
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);
        initializeChart();
        initializeData();
        initializeCheckboxs();

        return view;
    }

    private void initializeChart()
    {
        mLineChart.setBackgroundColor(getResources().getColor(android.R.color.white));
        mLineChart.setHighlightEnabled(false);
        mLineChart.setNoDataText("");
        mLineChart.setDescription("");
        mLineChart.setHardwareAccelerationEnabled(true);
        Legend l = mLineChart.getLegend();
        l.setFormSize(10f); // set the size of the legend forms/shapes
        l.setForm(Legend.LegendForm.CIRCLE); // set what type of form/shape should be used
        l.setTextSize(12f);
        l.setTextColor(Color.BLACK);
        mLineChart.setNoDataTextDescription("");
        mLineChart.setDrawBorders(false);
        //mLineChart.setGridBackgroundColor(getResources().getColor(R.color.chart_background));
        mLineChart.setDrawGridBackground(true);
        XAxis xAxis = mLineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        YAxis rightAxis = mLineChart.getAxisRight();
        rightAxis.setEnabled(false);
        YAxis leftAxis = mLineChart.getAxisLeft();
        leftAxis.setDrawAxisLine(true);
        leftAxis.setDrawGridLines(true);


        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);
        mLineChart.setAutoScaleMinMaxEnabled(true);

    }

    private void initializeCheckboxs()
    {
        mDollarBlueCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    mLineChart.getLineData().addDataSet(mDollarBlueDataSet);
                else
                    mLineChart.getLineData().removeDataSet(mDollarBlueDataSet);
                mLineChart.notifyDataSetChanged();
                mLineChart.invalidate();
            }
        });
        mDollarOfficialCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    mLineChart.getLineData().addDataSet(mDollarOfficialDataSet);
                else
                    mLineChart.getLineData().removeDataSet(mDollarOfficialDataSet);
                mLineChart.notifyDataSetChanged();
                mLineChart.invalidate();
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    private void initializeData()
    {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(ParseConstants.HISTORY_TABLE);
        query.addDescendingOrder(ParseConstants.CREATE_AT_KEY);
        query.setLimit(300);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                ArrayList<Entry> dollarBlueVals = new ArrayList<Entry>();
                ArrayList<Entry> dollarOfficialVals = new ArrayList<Entry>();


                int index = 0;
                int partialIndex = 0;
                ArrayList<String> xVals = new ArrayList<String>();
                int i = list.size() - 1;
                while (i >= 0) {
                    ParseObject item = list.get(i);
                    i--;
                    Entry entry = new Entry((float) item.getDouble(ParseConstants.PRICES_TABLE_SELL_KEY), index);
                    switch (item.getString(ParseConstants.PRICES_TABLE_TYPE_KEY)) {
                        case ParseConstants.DOLAR_BLUE_TYPE:
                            dollarBlueVals.add(entry);
                            break;
                        case ParseConstants.DOLAR_OFFICIAL_TYPE:
                            dollarOfficialVals.add(entry);
                            break;
                    }
                    partialIndex++;
                    if (partialIndex > BlueApp.COINS.length - 1) {
                        partialIndex = 0;
                        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                        Date newDate = null;
                        try {
                            newDate = format.parse(item.getString(ParseConstants.VALUE_DATE));
                            format = new SimpleDateFormat("dd/MM");
                            xVals.add(format.format(newDate));

                        } catch (java.text.ParseException e1) {
                            e1.printStackTrace();
                        }
                        index++;
                    }
                }
                mDollarBlueDataSet = new LineDataSet(dollarBlueVals, "Dolar Blue");
                initializeDataSet(mDollarBlueDataSet, getResources().getColor(R.color.blue_main));
                mDollarBlueDataSet.setValueTextSize(10);
                mDollarBlueDataSet.setDrawCircleHole(true);
                mDollarBlueDataSet.setCircleSize(5f);
                mDollarBlueDataSet.setLineWidth(4);
                mDollarOfficialDataSet = new LineDataSet(dollarOfficialVals, "Dolar Oficial");
                initializeDataSet(mDollarOfficialDataSet, getResources().getColor(R.color.green_official));
                mDollarOfficialDataSet.setValueTextSize(10);
                mDollarOfficialDataSet.setDrawCircleHole(true);
                mDollarOfficialDataSet.setCircleSize(5f);
                mDollarOfficialDataSet.setLineWidth(4);
                ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
                dataSets.add(mDollarBlueDataSet);
                dataSets.add(mDollarOfficialDataSet);
                LineData lineData = new LineData(xVals, dataSets);
                mLineChart.setData(lineData);
                mLineChart.setVisibleXRangeMaximum(7);
                mLineChart.moveViewToX(dollarBlueVals.size() - 7);
                mLineChart.invalidate();
                enableCheckboxs();
                crossfade();
            }
        });
    }


    private void crossfade() {

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        mContentView.setAlpha(0f);
        mContentView.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        mContentView.animate()
                .alpha(1f)
                .setDuration(mShortAnimationDuration)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        mLoadingView.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLoadingView.setVisibility(View.GONE);
                    }
                });
    }


    private void enableCheckboxs()
    {
        mDollarBlueCheckbox.setEnabled(true);
        mDollarOfficialCheckbox.setEnabled(true);
    }

    private void initializeDataSet(LineDataSet dataSet , int color)
    {
        dataSet.setDrawCubic(true);
        dataSet.setCircleColor(color);
        dataSet.setColor(color);
        dataSet.setDrawCircleHole(false);
        dataSet.setDrawFilled(false);
        dataSet.setHighlightEnabled(false);
    }

}
